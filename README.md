== How to build ? ==

```
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -GNinja
ninja -C .
```

== How to use ? ==

```
export VK_LAYER_PATH=$VK_LAYER_PATH:<path_to_vulkan_layer>
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<path_to_vulkan_layer>/build
export VK_INSTANCE_LAYERS=VK_LAYER_fake
```

The following message should be printed if the layer is found:

```
WARNING: Fake layer initialized
```
