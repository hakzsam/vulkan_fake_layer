/*
 * Copyright © 2021 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <vulkan/vulkan.h>
#include <vulkan/vk_layer.h>
#include "vk_layer_dispatch_table.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <mutex>
#include <unordered_map>
#include <atomic>

#undef VK_LAYER_EXPORT
#define VK_LAYER_EXPORT extern "C"

// global lock
std::mutex global_lock;
typedef std::lock_guard<std::mutex> lock_guard_t;
typedef std::unique_lock<std::mutex> unique_lock_t;

// atomic counter for pipeline tests
std::atomic<unsigned> pipeline_count;

// use the loader's dispatch table pointer as a key for dispatch map lookups
template<typename DispatchableType>
void *GetKey(DispatchableType inst)
{
  return *(void **)inst;
}

// instance and device dispatch tables
std::unordered_map<void *, VkLayerInstanceDispatchTable> instance_dispatch;
std::unordered_map<void *, VkLayerDispatchTable> device_dispatch;

static void
print_application_info(const VkApplicationInfo *app)
{
    fprintf(stderr, "pApplicationName: %s\n", app->pApplicationName);
    fprintf(stderr, "applicationVersion: %d\n", app->applicationVersion);
    fprintf(stderr, "pEngineName: %s\n", app->pEngineName);
    fprintf(stderr, "engineVersion: %d\n", app->engineVersion);
    fprintf(stderr, "apiVersion: %d\n", app->apiVersion);
    fprintf(stderr, "----------\n");
}

// instance chain
VK_LAYER_EXPORT VkResult VKAPI_CALL
fake_CreateInstance(
    const VkInstanceCreateInfo*                 pCreateInfo,
    const VkAllocationCallbacks*                pAllocator,
    VkInstance*                                 pInstance)
{
    VkResult result;

    VkLayerInstanceCreateInfo *layerCreateInfo =
        (VkLayerInstanceCreateInfo *)pCreateInfo->pNext;

    while (layerCreateInfo &&
           (layerCreateInfo->sType != VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO ||
            layerCreateInfo->function != VK_LAYER_LINK_INFO)) {
        layerCreateInfo = (VkLayerInstanceCreateInfo *)layerCreateInfo->pNext;
    }

    if (!layerCreateInfo)
        return VK_ERROR_INITIALIZATION_FAILED;

    PFN_vkGetInstanceProcAddr getInstanceProcAddr =
        layerCreateInfo->u.pLayerInfo->pfnNextGetInstanceProcAddr;

    layerCreateInfo->u.pLayerInfo = layerCreateInfo->u.pLayerInfo->pNext;

    PFN_vkCreateInstance createInstance = (PFN_vkCreateInstance)
        getInstanceProcAddr(VK_NULL_HANDLE, "vkCreateInstance");

    fprintf(stderr, "***********************************\n");
    fprintf(stderr, "* WARNING: Fake layer initialized *\n");
    fprintf(stderr, "***********************************\n");

    fprintf(stderr, "List of enabled instance extensions:\n");
    for (unsigned i = 0; i < pCreateInfo->enabledExtensionCount; i++) {
        fprintf(stderr, "-> %s\n", pCreateInfo->ppEnabledExtensionNames[i]);
    }

    if (pCreateInfo->pApplicationInfo) {
        fprintf(stderr, "Default application info: \n");
        print_application_info(pCreateInfo->pApplicationInfo);

        VkApplicationInfo fake_app_info = {
            .sType = pCreateInfo->pApplicationInfo->sType,
            .pNext = pCreateInfo->pApplicationInfo->pNext,
            .pApplicationName = "fake app",
            .applicationVersion = pCreateInfo->pApplicationInfo->applicationVersion,
            .pEngineName = "fake engine",
            .engineVersion = pCreateInfo->pApplicationInfo->engineVersion,
            .apiVersion = pCreateInfo->pApplicationInfo->apiVersion,
        };

        fprintf(stderr, "Fake application info: \n");
        print_application_info(&fake_app_info);

        VkInstanceCreateInfo fake_pCreateInfo;
        fake_pCreateInfo.sType = pCreateInfo->sType;
        fake_pCreateInfo.pNext = pCreateInfo->pNext;
        fake_pCreateInfo.flags = pCreateInfo->flags;
        fake_pCreateInfo.pApplicationInfo = &fake_app_info,
        fake_pCreateInfo.enabledLayerCount = pCreateInfo->enabledLayerCount;
        fake_pCreateInfo.ppEnabledLayerNames = pCreateInfo->ppEnabledLayerNames;
        fake_pCreateInfo.enabledExtensionCount = pCreateInfo->enabledExtensionCount;
        fake_pCreateInfo.ppEnabledExtensionNames = pCreateInfo->ppEnabledExtensionNames;

        result = createInstance(&fake_pCreateInfo, pAllocator, pInstance);
        if (result != VK_SUCCESS)
            return result;
    } else {
        result = createInstance(pCreateInfo, pAllocator, pInstance);
        if (result != VK_SUCCESS)
            return result;
    }

    // Create the dispatch instance table.
    VkLayerInstanceDispatchTable dispatchTable;
    dispatchTable.GetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)
        getInstanceProcAddr(*pInstance, "vkGetInstanceProcAddr");
    dispatchTable.DestroyInstance = (PFN_vkDestroyInstance)
        getInstanceProcAddr(*pInstance, "vkDestroyInstance");
    dispatchTable.EnumerateDeviceExtensionProperties = (PFN_vkEnumerateDeviceExtensionProperties)
        getInstanceProcAddr(*pInstance, "vkEnumerateDeviceExtensionProperties");
    dispatchTable.GetPhysicalDeviceQueueFamilyProperties = (PFN_vkGetPhysicalDeviceQueueFamilyProperties)
        getInstanceProcAddr(*pInstance, "vkGetPhysicalDeviceQueueFamilyProperties");

    lock_guard_t l(global_lock);
    instance_dispatch[GetKey(*pInstance)] = dispatchTable;

    return VK_SUCCESS;
}

VK_LAYER_EXPORT void VKAPI_CALL
fake_DestroyInstance(
    VkInstance                                  instance,
    const VkAllocationCallbacks                 *pAllocator)
{
    lock_guard_t l(global_lock);
    instance_dispatch[GetKey(instance)].DestroyInstance(instance, pAllocator);
    instance_dispatch.erase(GetKey(instance));
}

VK_LAYER_EXPORT VkResult VKAPI_CALL
fake_EnumerateInstanceLayerProperties(
    uint32_t                                    *pPropertyCount,
    VkLayerProperties                           *pProperties)
{
    if (pPropertyCount)
        *pPropertyCount = 1;

    if (pProperties) {
        strcpy(pProperties->layerName, "VK_LAYER_fake");
        strcpy(pProperties->description, "vulkan fake layer");
        pProperties->implementationVersion = 1;
        pProperties->specVersion = VK_MAKE_VERSION(1, 0, VK_HEADER_VERSION);
    }

    return VK_SUCCESS;
}

VK_LAYER_EXPORT VkResult VKAPI_CALL
fake_EnumerateInstanceExtensionProperties(
    const char                                  *pLayerName,
    uint32_t                                    *pPropertyCount,
    VkExtensionProperties                       *pProperties)
{
    if (pLayerName == NULL || strcmp(pLayerName, "VK_LAYER_fake"))
        return VK_ERROR_LAYER_NOT_PRESENT;

    if (pPropertyCount)
        *pPropertyCount = 0;

    return VK_SUCCESS;
}

// device chain
VK_LAYER_EXPORT VkResult VKAPI_CALL
fake_CreateDevice(
    VkPhysicalDevice                            physicalDevice,
    const VkDeviceCreateInfo*                   pCreateInfo,
    const VkAllocationCallbacks*                pAllocator,
    VkDevice*                                   pDevice)
{
    VkResult result;

    VkLayerDeviceCreateInfo *layerCreateInfo =
        (VkLayerDeviceCreateInfo *)pCreateInfo->pNext;

    while (layerCreateInfo &&
           (layerCreateInfo->sType != VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO ||
            layerCreateInfo->function != VK_LAYER_LINK_INFO)) {
        layerCreateInfo = (VkLayerDeviceCreateInfo *)layerCreateInfo->pNext;
    }

    if (!layerCreateInfo)
        return VK_ERROR_INITIALIZATION_FAILED;
  
    PFN_vkGetInstanceProcAddr getInstanceProcAddr =
        layerCreateInfo->u.pLayerInfo->pfnNextGetInstanceProcAddr;
    PFN_vkGetDeviceProcAddr getDeviceProcAddr =
        layerCreateInfo->u.pLayerInfo->pfnNextGetDeviceProcAddr;

    layerCreateInfo->u.pLayerInfo = layerCreateInfo->u.pLayerInfo->pNext;

    PFN_vkCreateDevice createDevice = (PFN_vkCreateDevice)
        getInstanceProcAddr(VK_NULL_HANDLE, "vkCreateDevice");

    fprintf(stderr, "List of enabled device extensions:\n");
    for (unsigned i = 0; i < pCreateInfo->enabledExtensionCount; i++) {
        fprintf(stderr, "-> %s\n", pCreateInfo->ppEnabledExtensionNames[i]);
    }

    result = createDevice(physicalDevice, pCreateInfo, pAllocator, pDevice);
    if (result != VK_SUCCESS)
        return result;

    // create the device dispatch table
    VkLayerDispatchTable dispatchTable;
    dispatchTable.GetDeviceProcAddr = (PFN_vkGetDeviceProcAddr)
        getDeviceProcAddr(*pDevice, "vkGetDeviceProcAddr");
    dispatchTable.DestroyDevice = (PFN_vkDestroyDevice)
        getDeviceProcAddr(*pDevice, "vkDestroyDevice");

    lock_guard_t l(global_lock);
    device_dispatch[GetKey(*pDevice)] = dispatchTable;

    return VK_SUCCESS;
}

VK_LAYER_EXPORT void VKAPI_CALL
fake_DestroyDevice(
    VkDevice                                    device,
    const VkAllocationCallbacks                 *pAllocator)
{
    lock_guard_t l(global_lock);
    device_dispatch[GetKey(device)].DestroyDevice(device, pAllocator);
    device_dispatch.erase(GetKey(device));
}

VK_LAYER_EXPORT VkResult VKAPI_CALL
fake_EnumerateDeviceLayerProperties(
    VkPhysicalDevice                            physicalDevice,
    uint32_t                                    *pPropertyCount,
    VkLayerProperties                           *pProperties)
{
    return fake_EnumerateInstanceLayerProperties(pPropertyCount,
                                                        pProperties);
}

VK_LAYER_EXPORT VkResult VKAPI_CALL
fake_EnumerateDeviceExtensionProperties(
    VkPhysicalDevice                            physicalDevice,
    const char                                  *pLayerName,
    uint32_t                                    *pPropertyCount,
    VkExtensionProperties                       *pProperties)
{
    if (pLayerName == NULL || strcmp(pLayerName, "VK_LAYER_fake")) {
        if (physicalDevice == VK_NULL_HANDLE)
            return VK_SUCCESS;

        VkResult result;

        lock_guard_t l(global_lock);
        result = instance_dispatch[GetKey(physicalDevice)].EnumerateDeviceExtensionProperties(physicalDevice, pLayerName, pPropertyCount, pProperties);
/*
        fprintf(stderr, "Number of device extensions supported (%d)\n", *pPropertyCount);

        if (pProperties) {
            for (unsigned i = 0; i < *pPropertyCount; i++) {
                fprintf(stderr, "-> %s\n", pProperties[i].extensionName);
                if (!strcmp(pProperties[i].extensionName, "VK_AMD_gpu_shader_half_float"))
                    strcpy(pProperties[i].extensionName, "VK_KHR_swapchain");
                if (!strcmp(pProperties[i].extensionName, "VK_AMD_gpu_shader_int16"))
                    strcpy(pProperties[i].extensionName, "VK_KHR_swapchain");
                if (!strcmp(pProperties[i].extensionName, "VK_AMD_memory_overallocation_behavior"))
                    strcpy(pProperties[i].extensionName, "VK_KHR_swapchain");
                if (!strcmp(pProperties[i].extensionName, "VK_KHR_driver_properties"))
                    strcpy(pProperties[i].extensionName, "VK_KHR_swapchain");

            }
        }
*/

        return result;
    }

    if (pPropertyCount)
        *pPropertyCount = 0;

    return VK_SUCCESS;
}

#define vk_foreach_struct(__iter, __start) \
   for (struct VkBaseOutStructure *__iter = (struct VkBaseOutStructure *)(__start); \
        __iter; __iter = __iter->pNext)

#define vk_foreach_struct_const(__iter, __start) \
   for (const struct VkBaseInStructure *__iter = (const struct VkBaseInStructure *)(__start); \
        __iter; __iter = __iter->pNext)

VK_LAYER_EXPORT void VKAPI_CALL
fake_GetPhysicalDeviceQueueFamilyProperties(
    VkPhysicalDevice physicalDevice,
    uint32_t *pCount,
    VkQueueFamilyProperties *pQueueFamilyProperties)
{
        lock_guard_t l(global_lock);
        instance_dispatch[GetKey(physicalDevice)].GetPhysicalDeviceQueueFamilyProperties(physicalDevice, pCount, pQueueFamilyProperties);
}

static PFN_vkVoidFunction
fake_GetDeviceProcAddr(VkDevice device, const char *pName);
static PFN_vkVoidFunction
fake_GetInstanceProcAddr(VkInstance instance, const char *pName);

#define FUNC(name) (void *)fake_##name

// list of functions that are intercepted by this layer
static const struct {
    const char *name;
    void *ptr;
} funcs[] = {
    { "vkGetInstanceProcAddr", FUNC(GetInstanceProcAddr) },
    { "vkEnumerateInstanceLayerProperties", FUNC(EnumerateInstanceLayerProperties) },
    { "vkEnumerateInstanceExtensionProperties", FUNC(EnumerateInstanceExtensionProperties) },
    { "vkCreateInstance", FUNC(CreateInstance) },
    { "vkDestroyInstance", FUNC(DestroyInstance) },
    { "vkGetDeviceProcAddr", FUNC(GetDeviceProcAddr) },
    { "vkEnumerateDeviceLayerProperties", FUNC(EnumerateDeviceLayerProperties) },
    { "vkEnumerateDeviceExtensionProperties", FUNC(EnumerateDeviceExtensionProperties) },
    { "vkGetPhysicalDeviceQueueFamilyProperties", FUNC(GetPhysicalDeviceQueueFamilyProperties) },
    { "vkCreateDevice", FUNC(CreateDevice) },
    { "vkDestroyDevice", FUNC(DestroyDevice) },
};

#undef FUNC

static PFN_vkVoidFunction
fake_GetDeviceProcAddr(
    VkDevice                                    device,
    const char                                  *pName)
{
    for (uint32_t i = 0; i < sizeof(funcs) / sizeof(funcs[0]); i++) {
        if (!strcmp(pName, funcs[i].name))
            return (PFN_vkVoidFunction)funcs[i].ptr;
    }

    lock_guard_t l(global_lock);
    return device_dispatch[GetKey(device)].GetDeviceProcAddr(device, pName);
}

static PFN_vkVoidFunction
fake_GetInstanceProcAddr(
    VkInstance                                  instance,
    const char                                  *pName)
{
    for (uint32_t i = 0; i < sizeof(funcs) / sizeof(funcs[0]); i++) {
        if (!strcmp(pName, funcs[i].name))
            return (PFN_vkVoidFunction)funcs[i].ptr;
    }

    lock_guard_t l(global_lock);
    return instance_dispatch[GetKey(instance)].GetInstanceProcAddr(instance, pName);
}

VK_LAYER_EXPORT VkResult VKAPI_CALL
fake_NegotiateLoaderLayerInterfaceVersion(
    VkNegotiateLayerInterface *pVersionStruct)
{
    if (pVersionStruct->loaderLayerInterfaceVersion > 2)
        pVersionStruct->loaderLayerInterfaceVersion = 2;

    pVersionStruct->pfnGetInstanceProcAddr = fake_GetInstanceProcAddr;
    pVersionStruct->pfnGetDeviceProcAddr = fake_GetDeviceProcAddr;
    pVersionStruct->pfnGetPhysicalDeviceProcAddr = NULL;

    return VK_SUCCESS;
}
